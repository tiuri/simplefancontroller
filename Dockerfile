# Based on https://github.com/zinen/docker-alpine-pigpiod/blob/main/Dockerfile
FROM debian:buster-slim as builder
# Install basic package to compile source code
RUN apt-get update -y \
    && apt-get -y install make wget unzip gcc
# Follow the install guide from creator of pigpio, http://abyz.me.uk/rpi/pigpio/download.html
RUN wget --output-document=download.zip https://github.com/joan2937/pigpio/archive/master.zip \
# Downloaded content is placed inside specific folder to not be depended of branch naming from repo
    && mkdir download \
    && unzip -d . download.zip
RUN cd /pigpio-master \
# Fix for compiling on Alpine, https://github.com/joan2937/pigpio/issues/107
    #&& sed -i -e 's/ldconfig/echo ldconfig disabled/g' Makefile \
    && make \
    && make install

FROM python:3.9.5-slim-buster
# Running pigpiod in foreground then container should request SIGKILL right away and not the default SIGTERM, wait 10 sec., and then SIGKILL.
STOPSIGNAL SIGKILL
ENV DEBIAN_FRONTEND noninteractive

COPY --from=builder /usr/local /usr/local

# Install utils
RUN apt-get update -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends gcc g++ make python3-dev libffi-dev libssl-dev libxml2-dev libxslt1-dev

RUN useradd -ms /bin/bash controller
RUN chmod g+rw /home && mkdir -p /home/controller

ENV HOME /home/controller
WORKDIR /home/controller

ENV PYTHONIOENCODING=utf-8

ENV TZ=Europe/Vienna
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN chown -R controller:controller /home/controller

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV SHELL /bin/bash

USER controller

RUN pip3 install --upgrade pip

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . /home/controller/simplefancontroller
USER root
RUN chown -R controller:controller /home/controller/simplefancontroller
RUN pigpiod
USER controller
RUN pip3 install pigpio
WORKDIR /home/controller/simplefancontroller/simplefancontroller
ENTRYPOINT ["python", "main.py"]
