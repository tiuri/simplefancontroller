#!/bin/bash

echo "Welcome to SimpleFanController Setup!"
echo "Please note that this is an early version and that I don't take any responsibility for damages caused by the program."

echo "Do you accept all responsibility for damages caused by SimpleFanController? Please enter '1' or '2' and confirm by hitting Enter."
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done

read -p "Enter the URL for Redis [localhost:6379]: " REDIS_URL
REDIS_URL=${REDIS_URL:-localhost}
echo -n "Enter the password for Redis: "
read -s REDIS_PASSWORD
echo
read -p "Enter the User ID to be used for accessing local files: " PUID
read -p "Enter the Group ID to be used for accessing local files: " PGID
read -p "Enter the path where all persistent data shall be stored: " DATA_PATH

printf "REDIS_URL=%s\nREDIS_PASSWORD=%s\nPUID=%s\nPGID=%s\nDATA_PATH=%s" "$REDIS_URL" "$REDIS_PASSWORD" "$PUID" "$PGID" "$DATA_PATH"> .env

read -p "All done. Press <Enter> to exit and start SimpleFanController."
