import unittest
import os

from simplefancontroller.api.user_manager import SFCUserManager
from simplefancontroller.settings import SFCAPIConfig
from testing.base import generate_user

USER_TABLE = SFCAPIConfig.db_user_table


class SFCUserManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.manager = SFCUserManager()

    def tearDown(self) -> None:
        self.manager.con.close()
        if os.path.exists(SFCAPIConfig.db_data_file):
            os.remove(SFCAPIConfig.db_data_file)
        if os.path.exists(SFCAPIConfig.db_user_file):
            os.remove(SFCAPIConfig.db_user_file)

    def query_by_name(self, tablename: str, name: str) -> list:
        with self.manager.con:
            return self.manager.con.execute(
                f"SELECT * FROM {tablename} WHERE name = ?", (name,)
            ).fetchall()


class EmptySFCUserManagerTest(SFCUserManagerTest):
    def test_add_single_user(self):
        user = generate_user()
        self.manager.add_user(user)
        data = self.query_by_name(USER_TABLE, user.name)
        self.assertEqual(1, len(data))

    def test_add_multiple_users(self):
        for _ in range(100):
            self.manager.add_user(generate_user())
        with self.manager.con:
            data = self.manager.con.execute(f"SELECT * FROM {USER_TABLE}").fetchall()
        self.assertEqual(100, len(data))

    def test_remove_missing_user(self):
        with self.assertRaises(ValueError):
            self.manager.delete_user("some-random-email")

    def test_get_missing_user(self):
        with self.assertRaises(ValueError):
            self.manager.get_user_by_mail("some-random-email")


class PrefilledSFCUserManagerTest(SFCUserManagerTest):
    def setUp(self) -> None:
        self.user = generate_user()
        self.manager = SFCUserManager()
        self.manager.add_user(self.user)

    def test_add_existing_user(self):
        with self.assertRaises(ValueError):
            self.manager.add_user(self.user)

    def test_remove_user(self):
        self.manager.delete_user(self.user.email)
        with self.manager.con:
            data = self.manager.con.execute(f"SELECT * FROM {USER_TABLE} WHERE email = ?", (self.user.email, )).fetchall()
        self.assertEqual(0, len(data))

    def test_get_multiple_users(self):
        # add another 99 users -> total: 100
        for _ in range(99):
            self.manager.add_user(generate_user())
        users = self.manager.get_users()
        self.assertEqual(100, len(list(users)))

    def test_check_password(self):
        check = self.manager.check_user_password(self.user.email, self.user.password)
        self.assertTrue(check)

    def test_check_invalid_password(self):
        check = self.manager.check_user_password(self.user.email, "wrong password")
        self.assertFalse(check)


if __name__ == "__main__":
    unittest.main()
