INFLUXDB_SETTINGS = {
    "name": "test-influxdb",
    "hostname": "localhost",
    "token": "test-token",
    "bucket": "test-bucket",
    "organisation": "test-organisation",
}
