from time import sleep
import unittest

from simplefancontroller.sfc.settings import SFC2PinFanSettings


class GeneralSFCFanTest(unittest.TestCase):
    def setUp(self) -> None:
        sleep(0.2)
        from testing.base import generate_2pin_fan

        self.fan = generate_2pin_fan()
        self.fan.settings.active = True
        self.fan.intensity = 0

    def tearDown(self) -> None:
        if not self.fan.device.closed:
            self.fan.shutdown()

    def test_stop(self):
        self.fan.stop()
        self.assertEqual(self.fan._is_stopped, True)

    def test_start(self):
        self.fan._is_stopped = True
        self.fan.start()
        self.assertEqual(self.fan._is_stopped, False)

    def test_shutdown(self):
        from testing.base import generate_2pin_fan

        self.fan.shutdown()
        self.assertTrue(self.fan.device.closed)
        # try generating the same fan again to detect if the pin is free again
        fan = generate_2pin_fan()
        fan.shutdown()

    def test_update_device_change_pin(self):
        self.assertEqual(self.fan.settings.gpio_pin, self.fan.device.pin.number)
        self.fan.settings.gpio_pin = 12
        self.fan.update_device()
        self.assertEqual(12, self.fan.device.pin.number)

    def test_replace_settings(self):
        new_settings = SFC2PinFanSettings(name=self.fan.settings.name, gpio_pin=13)
        self.assertEqual(self.fan.settings.gpio_pin, self.fan.device.pin.number)
        self.fan.settings = new_settings
        self.assertEqual(new_settings.gpio_pin, self.fan.device.pin.number)

    def test_connect(self):
        self.assertFalse(self.fan.device.closed)

    def test_state_on_100(self):
        self.fan.intensity = 100
        state = self.fan.state
        self.assertEqual(100.0, state.intensity)
        self.assertEqual(True, state.active)

    def test_inverse_full(self):
        self.fan.settings.invert_signal = True
        self.fan.intensity = 100
        self.fan._apply_intensity()
        self.assertEqual(self.fan.device.value, 0)

    def test_inverse_off(self):
        self.fan.settings.invert_signal = True
        self.fan.intensity = 0.0
        self.fan._apply_intensity()
        self.assertEqual(self.fan.device.value, 1)


class SFC2FanTest(GeneralSFCFanTest):
    def test_intensity_off_0(self):
        self.fan.update_status(temperature=0)
        self.assertEqual(0, self.fan.intensity)
        self.assertFalse(self.fan.device.is_active)

    def test_intensity_off_40(self):
        self.fan.update_status(temperature=40)
        self.assertEqual(0, self.fan.intensity)
        self.assertFalse(self.fan.device.is_active)

    def test_intensity_on_41(self):
        self.fan.update_status(temperature=41)
        self.assertEqual(100, self.fan.intensity)
        self.assertTrue(self.fan.device.is_active)

    def test_intensity_off_prev_on(self):
        self.fan.intensity = 100
        self.fan.update_status(temperature=39)
        self.assertEqual(100, self.fan.intensity)
        self.assertTrue(self.fan.device.is_active)

    def test_intensity_off(self):
        self.fan.intensity = 100
        self.fan.settings.active = False
        self.fan.update_status(temperature=40)
        self.assertFalse(self.fan.device.is_active)


class SFCPWMFanTest(GeneralSFCFanTest):
    def setUp(self) -> None:
        from testing.base import generate_pwm_fan

        self.fan = generate_pwm_fan()
        self.fan.settings.active = True

    def test_intensity_40(self):
        self.fan.update_status(30)
        self.assertEqual(0, self.fan.intensity)
        self.assertEqual(0, self.fan.device.value)

    def test_intensity_60(self):
        self.fan.update_status(60)
        self.assertEqual(50, self.fan.intensity)
        self.assertEqual(0.5, self.fan.device.value)

    def test_intensity_80(self):
        self.fan.update_status(80)
        self.assertEqual(100, self.fan.intensity)
        self.assertEqual(1, self.fan.device.value)


class SFCPWMReverseFanTest(GeneralSFCFanTest):
    def setUp(self) -> None:
        from testing.base import generate_pwm_fan

        self.fan = generate_pwm_fan()
        self.fan.settings.active = True
        self.fan.settings.invert_signal = True

    def test_intensity_reverse_40(self):
        self.fan.update_status(30)
        self.assertEqual(100, self.fan.intensity)
        self.assertEqual(1, self.fan.device.value)

    def test_intensity_reverse_60(self):
        self.fan.update_status(60)
        self.assertEqual(50, self.fan.intensity)
        self.assertEqual(0.5, self.fan.device.value)

    def test_intensity_reverse_80(self):
        self.fan.update_status(80)
        self.assertEqual(0, self.fan.intensity)
        self.assertEqual(0, self.fan.device.value)


if __name__ == "__main__":
    unittest.main()
