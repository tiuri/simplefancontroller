import unittest
from mock import patch
from time import sleep
import os

from simplefancontroller.sfc.controller import SimpleFanController
from testing.base import TemperatureMock, generate_sfc


tmp_mock = TemperatureMock()


class ThreadManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        sleep(0.2)
        with patch.object(
            SimpleFanController, "get_temperature"
        ) as self.temp_mock, patch.object(
            SimpleFanController, "set_fan_intensities"
        ) as self.fan_mock:
            self.sfc = generate_sfc()

    def tearDown(self) -> None:
        self.sfc.shutdown()
        if os.path.exists("temp.txt"):
            os.remove("temp.txt")

    def test_start(self):
        self.sfc.thread_manager.start()
        self.temp_mock.assert_called()
        self.fan_mock.assert_called()
        self.assertTrue(self.sfc.thread_manager._thread.is_alive())

    def test_stop(self):
        self.sfc.thread_manager.start()
        self.sfc.thread_manager.stop()
        sleep(self.sfc.thread_manager._internal_sleep_time + 0.1)
        self.assertIsNone(self.sfc.thread_manager._thread)

    def test_restart(self):
        old_id = id(self.sfc.thread_manager._thread)
        self.sfc.thread_manager.restart()
        sleep(self.sfc.thread_manager._internal_sleep_time + 0.1)
        new_id = id(self.sfc.thread_manager._thread)
        self.assertNotEqual(old_id, new_id)


if __name__ == "__main__":
    unittest.main()
