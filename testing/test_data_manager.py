import sqlite3
import unittest
import os

from simplefancontroller.settings import SFCAPIConfig
from simplefancontroller.sfc.data_manager import SFCDataManager
from simplefancontroller.sfc.settings import SFControllerSettings, SFCFanSettings
from testing.base import generate_2pin_fan, generate_pwm_fan, generate_influxdb_settings

FAN_TABLE = SFCAPIConfig.db_fan_table
SETTINGS_TABLE = SFCAPIConfig.db_settings_table
PERSISTENCE_TABLE = SFCAPIConfig.db_persistence_table


class SFCDataManagerTest(unittest.TestCase):
    manager: SFCDataManager

    def setUp(self) -> None:
        self.manager = SFCDataManager()

    def tearDown(self) -> None:
        self.manager.con.close()
        if os.path.exists(SFCAPIConfig.db_data_file):
            os.remove(SFCAPIConfig.db_data_file)
        if os.path.exists(SFCAPIConfig.db_user_file):
            os.remove(SFCAPIConfig.db_user_file)

    def query_by_name(self, tablename: str, name: str) -> list:
        with self.manager.con:
            return self.manager.con.execute(
                f"SELECT * FROM {tablename} WHERE name = ?", (name,)
            ).fetchall()

    def test_shutdown(self):
        self.manager.shutdown()
        with self.assertRaises(sqlite3.ProgrammingError):
            list(self.manager.get_fans())


class EmptySFCDataManagerTest(SFCDataManagerTest):
    def test_export_2pinfan(self):
        fan = generate_2pin_fan()
        fan.shutdown()
        self.manager.save_fan(fan.settings)
        data = self.query_by_name(FAN_TABLE, fan.settings.name)
        self.assertEqual(1, len(data))

    def test_export_pwmfan(self):
        fan = generate_pwm_fan()
        fan.shutdown()
        self.manager.save_fan(fan.settings)
        data = self.query_by_name(FAN_TABLE, fan.settings.name)
        self.assertEqual(1, len(data))

    def test_export_settings(self):
        settings = SFControllerSettings()
        self.manager.save_settings(settings)
        with self.manager.con:
            data = self.manager.con.execute(
                f"SELECT * FROM {SETTINGS_TABLE}"
            ).fetchall()
        self.assertEqual(1, len(data))

    def test_export_db_settings(self):
        db_settings = generate_influxdb_settings()
        self.manager.save_persistence(db_settings)
        data = self.query_by_name(PERSISTENCE_TABLE, db_settings.name)
        self.assertEqual(1, len(data))

    def test_get_settings(self):
        with self.assertRaises(ValueError):
            self.manager.get_settings()


class PreFilledSFCDataManagerTest(SFCDataManagerTest):
    def setUp(self) -> None:
        self.manager = SFCDataManager()
        fans = [generate_2pin_fan(), generate_pwm_fan()]
        self.fans = {x.settings.name: x.settings for x in fans}
        for fan in fans:
            fan.shutdown()
            self.manager.save_fan(fan.settings)
        self.manager.save_settings(SFControllerSettings())
        self.manager.save_persistence(generate_influxdb_settings())

    def test_update_existing_fan(self):
        updated = generate_pwm_fan()
        updated.settings.pwm_frequency = 123456
        self.manager.save_fan(updated.settings)
        data = self.query_by_name(FAN_TABLE, updated.settings.name)
        self.assertEqual(1, len(data))
        self.assertTrue("123456" in data[0][3])

    def test_get_fan(self):
        for name, fan in self.fans.items():
            queried = self.manager.get_fan(name)
            self.assertEqual(fan, queried)

    def test_get_missing_fan(self):
        with self.assertRaises(ValueError):
            self.manager.get_fan("some-random-name")

    def test_get_fans(self):
        data = list(self.manager.get_fans())
        self.assertEqual(len(self.fans), len(data))
        for fan in data:
            self.assertTrue(any(map(lambda x: x == fan, self.fans.values())))

    def test_get_fan_names(self):
        data = list(self.manager.get_fan_names())
        self.assertEqual(len(self.fans), len(data))
        for fan in data:
            self.assertTrue(any(map(lambda x: x == fan, self.fans.keys())))

    def test_delete_fan(self):
        for name in self.fans.keys():
            self.manager.delete_fan(name)
        data = list(self.manager.get_fans())
        self.assertEqual(0, len(data))

    def test_delete_missing_fan(self):
        with self.assertRaises(ValueError):
            self.manager.delete_fan("some-random-name")

    def test_update_settings(self):
        new_settings = SFControllerSettings()
        new_settings.active = True
        self.manager.save_settings(new_settings)
        data = self.manager.get_settings()
        self.assertEqual(True, data.active)

    def test_get_settings(self):
        data = self.manager.get_settings()
        self.assertIsInstance(data, SFControllerSettings)
        self.assertEqual(False, data.active)

    def test_delete_settings(self):
        self.manager.delete_settings()
        with self.assertRaises(ValueError):
            self.manager.get_settings()

    def test_add_persistence(self):
        new_settings = generate_influxdb_settings()
        new_settings.name = "new-influxdb"
        self.manager.save_persistence(new_settings)
        data = list(self.manager.get_persistence_names())
        self.assertEqual(2, len(data))

    def test_get_missing_persistence(self):
        with self.assertRaises(ValueError):
            self.manager.get_persistence("some-random-name")

    def test_delete_persistence(self):
        name = generate_influxdb_settings().name
        self.manager.delete_persistence(name)
        data = self.query_by_name(PERSISTENCE_TABLE, name)
        self.assertEqual(0, len(data))

    def test_update_persistence(self):
        new_settings = generate_influxdb_settings()
        new_settings.active = True
        self.manager.save_persistence(new_settings)
        data = self.manager.get_persistence(new_settings.name)
        self.assertEqual(True, data.active)

    def test_get_persistence(self):
        old_data = generate_influxdb_settings()
        data = self.manager.get_persistence(old_data.name)
        self.assertEqual(old_data, data)


if __name__ == "__main__":
    unittest.main()
