import unittest
import os
import pathlib
from time import sleep

from simplefancontroller.sfc.database import SFCInfluxDBClient
from simplefancontroller.sfc.settings import SFControllerSettings
from simplefancontroller.sfc.controller import SimpleFanController

from testing.base import generate_sfc, TemperatureMock, generate_influxdb_settings


class SimpleFanControllerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.sfc = generate_sfc()

    def tearDown(self) -> None:
        self.sfc.shutdown()

    def test_get_temperature(self):
        os.environ["SFC_DEBUG"] = "false"
        with TemperatureMock() as temp:
            self.sfc.settings.sensor_file = temp.filename
            self.sfc.get_temperature()
            self.assertNotEqual(0.0, self.sfc.current_temperature)

    def test_set_fan_intensities(self):
        f1_intensities = [0, 100, 100]
        f2_intensities = [0, 50, 100]

        for idx, temp in enumerate([30, 60, 80]):
            self.sfc.current_temperature = temp
            self.sfc.set_fan_intensities()

            for fan in self.sfc.fans.get_fans():
                if "1" in fan.settings.name:
                    self.assertEqual(f1_intensities[idx], fan.intensity)
                else:
                    self.assertEqual(f2_intensities[idx], fan.intensity)

    def test_export_settings(self):
        self.sfc.persistence_settings = [generate_influxdb_settings()]
        self.sfc.export_data()

    def test_import_settings(self):
        self.sfc.shutdown()
        self.sfc = SimpleFanController(SFControllerSettings(active=True))
        self.sfc.import_data()

    def test_add_db_client(self):
        influx = generate_influxdb_settings()
        self.sfc.attach_db_client(influx)
        self.assertEqual(1, len(self.sfc.db_clients))
        self.assertIsInstance(list(self.sfc.db_clients.values())[0], SFCInfluxDBClient)

    def test_add_existing_db_client(self):
        self.test_add_db_client()
        with self.assertRaises(ValueError):
            self.test_add_db_client()

    def test_remove_db_client(self):
        self.test_add_db_client()
        self.sfc.remove_db_client("test-influxdb")
        self.assertEqual(0, len(self.sfc.db_clients))

    def test_remove_missing_db_client(self):
        with self.assertRaises(ValueError):
            self.sfc.remove_db_client("some-random-name")

    def test_get_db_client(self):
        self.test_add_db_client()
        self.sfc.get_db_client("test-influxdb")

    def test_get_missing_db_client(self):
        with self.assertRaises(ValueError):
            self.sfc.get_db_client("some-random-name")

    def test_update_db_client(self):
        self.test_add_db_client()
        new_settings = generate_influxdb_settings()
        new_name = "test-influxdb-2"
        new_settings.name = new_name
        self.sfc.update_db_client("test-influxdb", new_settings)
        self.assertEqual(1, len(self.sfc.db_clients))
        self.assertTrue(new_name in self.sfc.db_clients)
        self.assertTrue(self.sfc.db_clients[new_name].settings == new_settings)

    def test_update_missing_db_client(self):
        with self.assertRaises(ValueError):
            self.sfc.update_db_client("some-random-name", generate_influxdb_settings())

    def test_stop(self):
        self.sfc.stop()

    def test_shutdown(self):
        self.sfc.shutdown()


if __name__ == "__main__":
    unittest.main()
