import os
import random
import string

import numpy as np
from gpiozero.devices import Device
from gpiozero.pins.mock import MockPin, MockPWMPin, MockFactory
import names

from simplefancontroller.sfc import SFCPWMFan, SFC2PinFan
from simplefancontroller.sfc.settings import SFC2PinFanSettings, SFCPWMFanSettings
from simplefancontroller.sfc.controller import SimpleFanController
from simplefancontroller.sfc.database import SFCInfluxDBSettings
from simplefancontroller.sfc.settings import SFControllerSettings
from simplefancontroller.api.schemas import User
from .debug_settings import INFLUXDB_SETTINGS


def generate_2pin_fan(name: str = "test-fan-1", pin: int = 10) -> SFC2PinFan:
    """Generates a SFC2PinFan with a MockPin"""
    Device.pin_factory = MockFactory(pin_class=MockPin)
    return SFC2PinFan(settings=SFC2PinFanSettings(name=name, gpio_pin=pin))


def generate_pwm_fan(name: str = "test-fan-2", pin: int = 11) -> SFCPWMFan:
    """Generates a SFCPWMFan with a MockPin"""
    Device.pin_factory = MockFactory(pin_class=MockPWMPin)
    return SFCPWMFan(settings=SFCPWMFanSettings(name=name, gpio_pin=pin))


def generate_sfc() -> SimpleFanController:
    """Generates a basic SimpleFanController with mocked a SFC2PinFan and a SFCPWMFan."""
    ctl = SimpleFanController(SFControllerSettings(active=True))
    f1 = generate_pwm_fan()
    f2 = generate_2pin_fan()
    for fan in [f1, f2]:
        ctl.fans.add_fan(fan)
    ctl.start()
    return ctl


def generate_influxdb_settings():
    """Generates InfluxDB for local dev"""
    return SFCInfluxDBSettings(**INFLUXDB_SETTINGS)


def generate_user():
    first = names.get_first_name()
    last = names.get_last_name()
    email = f"{first.lower()}.{last.lower()}@testmail.com"
    password = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
    return User(
        name=f"{first} {last}",
        email=email,
        password=password
    )


class TemperatureMock:
    def __init__(self):
        self.filename = "temp.txt"
        self.generate_temperature_file()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.filename)

    @staticmethod
    def generate_temperature() -> str:
        return str(np.random.randint(40, 80) * 10**3)

    def generate_temperature_file(self):
        if os.path.exists(self.filename):
            os.remove(self.filename)

        with open(self.filename, "w") as file:
            file.write(self.generate_temperature())
