import unittest
from time import sleep

from simplefancontroller.sfc.storage import SFCFanStorage
from simplefancontroller.sfc.fans import SFCFan
from simplefancontroller.sfc.data_manager import SFCDataManager
from testing.base import generate_pwm_fan, generate_2pin_fan


FAN_NAME_1 = "test-1"
FAN_NAME_2 = "test-2"


class FanStorageTest(unittest.TestCase):
    def setUp(self) -> None:
        sleep(0.2)
        self.manager = SFCFanStorage(SFCDataManager())
        self.f1 = f1 = generate_2pin_fan(FAN_NAME_1)
        f1.settings.gpio_pin = 10
        self.manager.add_fan(f1)
        self.f2 = f2 = generate_pwm_fan(FAN_NAME_2)
        f2.settings.gpio_pin = 11
        self.manager.add_fan(f2)

    def tearDown(self) -> None:
        self.manager.shutdown()

    def test_get_fan(self):
        fan = self.manager.get_fan(FAN_NAME_1)
        self.assertEqual(self.f1, fan)

    def test_get_fan_invalid_name(self):
        with self.assertRaises(ValueError):
            self.manager.get_fan("invalid")

    def test_get_fan_names(self):
        fan_names = [FAN_NAME_1, FAN_NAME_2]
        for index, fan_name in enumerate(self.manager.get_fan_names()):
            self.assertEqual(fan_names[index], fan_name)

    def test_update_changed_pin(self):
        tmp_fan = generate_pwm_fan(FAN_NAME_1, pin=14)
        tmp_fan.shutdown()
        self.manager.update_fan(FAN_NAME_1, tmp_fan.settings)
        new_fan = self.manager.get_fan(FAN_NAME_1)
        self.assertEqual(14, new_fan.settings.gpio_pin)
        self.assertEqual(14, new_fan.device.pin.number)

    def test_update_changed_name(self):
        new_name = "test-fan-3"
        tmp_fan = generate_pwm_fan(new_name, pin=15)
        tmp_fan.shutdown()
        self.manager.update_fan(FAN_NAME_1, tmp_fan.settings)
        new_fan = self.manager.get_fan(new_name)
        self.assertEqual(new_name, new_fan.settings.name)

    def test_regular_pins_occupied(self):
        pins = list(self.manager.get_occupied_pins())
        self.assertCountEqual([10, 11], pins)

    def test_add_existing_name(self):
        f3 = generate_pwm_fan(FAN_NAME_1, pin=15)
        f3.shutdown()
        with self.assertRaises(ValueError):
            self.manager.add_fan(f3)

    def test_remove_fan(self):
        self.manager.remove_fan(FAN_NAME_1)
        self.assertEqual(1, len(self.manager._container))
        self.assertEqual(FAN_NAME_2, self.manager._container[0].settings.name)

    def test_get_fans(self):
        fans = list(self.manager.get_fans())
        for fan in fans:
            self.assertIsInstance(fan, SFCFan)
        self.assertEqual(2, len(list(fans)))

    def test_get_fan_status(self):
        fan1 = self.manager.get_fan(FAN_NAME_1)
        fan1.settings.active = False
        status = self.manager.get_state()
        for fan_name, fan_status in status:
            if fan_name == FAN_NAME_1:
                self.assertEqual(False, fan_status.active)
                self.assertEqual(False, fan_status.is_stopped)
            elif fan_name == FAN_NAME_2:
                self.assertEqual(True, fan_status.active)

    def test_add_fan_prv_occupied_pin(self):
        old_fan = self.manager.get_fan(FAN_NAME_1)
        pin = old_fan.settings.gpio_pin
        self.manager.remove_fan(FAN_NAME_1)
        new_fan = generate_2pin_fan(name=FAN_NAME_1, pin=pin)
        self.manager.add_fan(new_fan)

    def test_add_from_settings(self):
        new_name = "test-fan-3"
        tmp_fan = generate_pwm_fan(new_name, 15)
        tmp_fan.shutdown()
        self.manager.add_fan(SFCFan.from_settings(tmp_fan.settings))
        fan = self.manager.get_fan(new_name)
        self.assertEqual(fan.device.pin.number, 15)

    def test_shutdown(self):
        self.manager.shutdown()
        for fan in self.manager._container:
            self.assertTrue(fan.device.closed)

    def test_start(self):
        self.manager.start()

    def test_stop(self):
        self.manager.stop()


if __name__ == "__main__":
    unittest.main()
