.. SimpleFanController documentation master file, created by
   sphinx-quickstart on Fri Jul  8 19:05:20 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SimpleFanController's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

SimpleFanController is a service for controlling and managing fans connected to the GPIO pins of a RaspberryPi.

It's key features are:

- Support for 2-pin and 4-pin (PWM) fans
- Monitoring via a InfluxDB database connector
- REST interface following the OpenAPI standard
- Bearer token user authentication (OAuth2)
- Provided as Python library and Docker images for flexible deployment


.. warning::
   SimpleFanController is still under active development and not stable yet!

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
