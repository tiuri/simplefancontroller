simplefancontroller.api package
===============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   simplefancontroller.api.routers

Submodules
----------

simplefancontroller.api.app module
----------------------------------

.. automodule:: simplefancontroller.api.app
   :members:
   :undoc-members:
   :show-inheritance:

simplefancontroller.api.jwt\_bearer module
------------------------------------------

.. automodule:: simplefancontroller.api.jwt_bearer
   :members:
   :undoc-members:
   :show-inheritance:

simplefancontroller.api.schemas module
--------------------------------------

.. automodule:: simplefancontroller.api.schemas
   :members:
   :undoc-members:
   :show-inheritance:

simplefancontroller.api.user\_manager module
--------------------------------------------

.. automodule:: simplefancontroller.api.user_manager
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: simplefancontroller.api
   :members:
   :undoc-members:
   :show-inheritance:
