simplefancontroller.api.routers package
=======================================

Submodules
----------

simplefancontroller.api.routers.auth module
-------------------------------------------

.. automodule:: simplefancontroller.api.routers.auth
   :members:
   :undoc-members:
   :show-inheritance:

simplefancontroller.api.routers.controller module
-------------------------------------------------

.. automodule:: simplefancontroller.api.routers.controller
   :members:
   :undoc-members:
   :show-inheritance:

simplefancontroller.api.routers.fans module
-------------------------------------------

.. automodule:: simplefancontroller.api.routers.fans
   :members:
   :undoc-members:
   :show-inheritance:

simplefancontroller.api.routers.settings module
-----------------------------------------------

.. automodule:: simplefancontroller.api.routers.settings
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: simplefancontroller.api.routers
   :members:
   :undoc-members:
   :show-inheritance:
